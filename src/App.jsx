import Magnet from "./components/magnet/Magnet";
import { Cursor } from "./components/cursor/Cursor";
import './index.css';

const App = () => (
  <>
    <Cursor/>
    <div className="wrapper">
      <Magnet>
        <button className="btn">Сохранить</button>
      </Magnet>
      <Magnet>
        <button className="btn">Сохранить</button>
      </Magnet>
      <Magnet>
        <button className="btn">Сохранить</button>
      </Magnet>
      <button className="btn">НЕ МАГНЕТ</button>
    </div>
    <Magnet>
        <div className="welcome"> Hi there</div>
    </Magnet>
  </>
);

export default App;