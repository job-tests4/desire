import { configureStore } from "@reduxjs/toolkit";
import { cursorSlice } from "./slices/cursorSlice";

export const store = configureStore({
    reducer: {
        cursor: cursorSlice.reducer
    }
})