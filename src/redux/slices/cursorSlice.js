import { createSlice } from "@reduxjs/toolkit"

export const defaultSize = 50;

const initialState = {
    x: 0,
    y: 0,
    width: defaultSize,
    height: defaultSize,
    isMagnetizable: false,
}

export const cursorSlice = createSlice({
    name: 'cursor',
    initialState,
    reducers: {
        changePosition: (state, {payload}) => {
            state.x = payload.x;
            state.y = payload.y;
        },
        magnetCursor: (state, {payload}) => {
            state.x = payload.x;
            state.y = payload.y;
            state.isMagnetizable = true;
            state.width = payload.width;
            state.height = payload.height;
        },
        leaveMagnet: (state) => {
            state.height = defaultSize;
            state.width = defaultSize;
            state.isMagnetizable = false;
        }
    }
})

export const { changePosition, magnetCursor, leaveMagnet } = cursorSlice.actions;

export const selectCursor = (state) => state.cursor;