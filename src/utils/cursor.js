export const addCursor = () => {
    const styleSheet = document.createElement('style');
    styleSheet.innerText = `
        * { cursor: url('./cursor.png'), auto; }
        @media screen and (max-width: 1024px) {
          * { cursor: default; }
        }
      `;
    return styleSheet;
  };