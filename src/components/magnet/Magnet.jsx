import React from 'react';
import { useDispatch } from 'react-redux';
import { magnetCursor, leaveMagnet } from '../../redux/slices/cursorSlice';
import './style.css'

const Magnet = ({ children }) => {
  const dispatch = useDispatch();

  const onMouseEnter = (event) => {
    const element = event.target;
      if(element) {
          const { width, height, x, y } = element.getBoundingClientRect();

          dispatch(magnetCursor({
            x: x + window.scrollX,
            y: y + window.scrollY,
            width,
            height
          }))
      }
  } 

  const onMouseLeave = () => {
    dispatch(leaveMagnet());
  }

  return (
    <div 
      className='magnet' 
      onMouseEnter={onMouseEnter} 
      onMouseLeave={onMouseLeave}
    >
      {children}
    </div>
  );
};

export default Magnet;
