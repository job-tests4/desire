import React, { useEffect, useCallback, useRef } from 'react';
import { Scope } from '../scope/Scope';
import { useDispatch, useSelector } from 'react-redux';
import { changePosition, selectCursor } from '../../redux/slices/cursorSlice';
import { addCursor } from '../../utils/cursor';
import './style.css';

export const Cursor = () => {
  const dispatch = useDispatch();
  const { x, y, width, height, isMagnetizable } = useSelector(selectCursor);
  const ref = useRef({x: 0, y: 0});

  const changeSize = useCallback(({clientY, clientX}) => {
    if (!isMagnetizable && clientX && clientY) {
      ref.current = { x: clientX, y: clientY };
      dispatch(changePosition({
        x: clientX + window.scrollX,
        y: clientY + window.scrollY
      }));
    }
  }, [isMagnetizable, dispatch]);

  const handleScroll = useCallback(() => {
    if(ref.current && !isMagnetizable) {
      const newX = ref.current.x + window.scrollX;
      const newY = ref.current.y + window.scrollY;

      dispatch(changePosition({x: newX, y: newY}))
    }
  }, [isMagnetizable, dispatch])

  useEffect(() => {
    window.addEventListener('mousemove', changeSize);

    return () => {
      window.removeEventListener('mousemove', changeSize);
    };
  }, [changeSize]);

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    }
  }, [handleScroll])

  useEffect(() => {
    const styleSheet = addCursor();
    document.head.appendChild(styleSheet);

    return () => {
      document.head.removeChild(styleSheet);
    };
  }, []);

  return (
    <div
      className={`custom-cursor ${isMagnetizable ? 'magnetizable' : ''}`}
      style={{
        left: x,
        top: y,
      }}
    >
      {["first", "second", "last"].map((name) => (
        <Scope
          key={name}
          width={width}
          height={height}
          className={
            `scope ${isMagnetizable ? `animate-cursor-` : ''}${name}`
          }
        />
      ))}
    </div>
  );
};  