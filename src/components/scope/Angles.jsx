export const TopRight = () => (
    <div className="top-right">
      <svg className="corner-svg" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
        <path d="M0.300781 1H15.0008V15.7" stroke="white" stroke-width="2"/>
      </svg>
    </div>
)

export const TopLeft = () => (
  <div className="top-left">
    <svg className="corner-svg" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none" >
      <path d="M15.6992 1H0.999219V15.7" stroke="white" stroke-width="2"/>
    </svg>
  </div>
)
  
export const BotRight = () => (
  <div className="bottom-right">
    <svg className="corner-svg" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
      <path d="M0.300781 15H15.0008V0.299999" stroke="white" stroke-width="2"/>
    </svg>
  </div>
)
  
export const BotLeft = () => (
  <div className="bottom-left">
    <svg className="corner-svg" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
      <path d="M15.6992 15H0.999219V0.299999" stroke="white" stroke-width="2"/>
    </svg>
  </div>
)