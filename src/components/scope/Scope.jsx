import { BotLeft, BotRight, TopLeft, TopRight } from './Angles';

export const Scope = ({ className, width, height }) => (
    <div className={className} style={{width, height}}>
        <TopLeft />
        <TopRight />
        <BotLeft />
        <BotRight />
    </div>
)